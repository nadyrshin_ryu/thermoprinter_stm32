//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x_gpio.h>
#include <stm32f10x_usart.h>
#include <stm32f10x_rcc.h>
#include <delay.h>
#include "uart.h"


static uint32_t TOcntr;
static USART_TypeDef* pUART;


//==============================================================================
// ������������� UART. RxFunc - �������, ���������� ��� ��������� ������ � ��������� Nextion
//==============================================================================
void UART_Init(USART_TypeDef* USARTx, uint32_t BaudRate)
{
  pUART = USARTx;
  
  USART_Cmd(pUART, DISABLE);
  USART_DeInit(pUART);
  
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  if (pUART == USART1)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
  else
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

  // ����������� ���� Tx,Rx
  GPIO_InitTypeDef initGPIOStruct;
  initGPIOStruct.GPIO_Speed = GPIO_Speed_50MHz;
  
  if (pUART == USART1)
    initGPIOStruct.GPIO_Pin = GPIO_Pin_9;
  else
    initGPIOStruct.GPIO_Pin = GPIO_Pin_2;
  initGPIOStruct.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOA, &initGPIOStruct);
  
  if (pUART == USART1)
    initGPIOStruct.GPIO_Pin = GPIO_Pin_10;
  else
    initGPIOStruct.GPIO_Pin = GPIO_Pin_3;
  initGPIOStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOA, &initGPIOStruct);
  
  // ����������� USART
  USART_InitTypeDef initStruct;
  initStruct.USART_BaudRate = BaudRate;
  initStruct.USART_WordLength = USART_WordLength_8b;
  initStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  initStruct.USART_Parity = USART_Parity_No;
  initStruct.USART_StopBits = USART_StopBits_1;
  initStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_Init(pUART, &initStruct);

  USART_ClockInitTypeDef initClockStruct;
  initClockStruct.USART_Clock = USART_Clock_Disable;
  initClockStruct.USART_CPHA = USART_CPHA_1Edge;
  initClockStruct.USART_CPOL = USART_CPOL_High;
  initClockStruct.USART_LastBit = USART_LastBit_Disable;
  USART_ClockInit(pUART, &initClockStruct);
  
  // ��������� ������ USART
  USART_Cmd(pUART, ENABLE);
}
//==============================================================================


//==============================================================================
// ������� �������� ������ (�������� � UART ��������� ���������)
//==============================================================================
int8_t UART_Send(uint8_t *pBuff, uint16_t Len)
{
  while (Len--)
  {
    USART_SendData(pUART, *(pBuff++));

    TOcntr = UART_TX_TIMEOUT;
    while ((USART_GetFlagStatus(pUART, USART_FLAG_TXE) == RESET) && TOcntr) 
    {
      TOcntr--;
      delay_us(10);
    }
    if (!TOcntr)
      return UART_ERR_HW_TIMEOUT;
  }
  
  return UART_ERR_OK;
}
//==============================================================================


//==============================================================================
// ������� ������ ������
//==============================================================================
int8_t UART_Recv(uint8_t *pBuff, uint16_t Len)
{
  while (Len--)
  {
    uint8_t ch = USART_ReceiveData(pUART);
    *(pBuff++) = ch;
    
    TOcntr = UART_TX_TIMEOUT;
    while ((USART_GetFlagStatus(pUART, USART_FLAG_RXNE) == RESET) && TOcntr) 
    {
      TOcntr--;
      delay_us(10);
    }
    if (!TOcntr)
      return UART_ERR_HW_TIMEOUT;
  }
  
  return UART_ERR_OK;
}
//==============================================================================
