//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _UART_H
#define _UART_H


#define UART_TX_TIMEOUT         500      // ������� � 10 ��� ���������� �������� �������� �����

// ������������ ���� ������
#define UART_ERR_OK             0       // ��� ������ - ��������� ��� ������
#define UART_ERR_BUFF_OVF       (-1)    // ��� ������ - ������������ ������
#define UART_ERR_HW_TIMEOUT     (-2)    // ��� ������ - ���� ����-��� �������������� ��������


// ������������� UART
void UART_Init(USART_TypeDef* USARTx, uint32_t BaudRate);
// ������� �������� ������
int8_t UART_Send(uint8_t *pBuff, uint16_t Len);
// ������� ������ ������
int8_t UART_Recv(uint8_t *pBuff, uint16_t Len);


#endif