//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x.h>
#include <stm32f10x_gpio.h>
#include <string.h>
#include <uart.h>
#include <gpio.h>
#include <timers.h>
#include <delay.h>
#include <thermal_printer.h>
#include "main.h"


#define LedState_On()           GPIO_ResetBits(LedState_Port, LedState_Pin)
#define LedState_Off()          GPIO_SetBits(LedState_Port, LedState_Pin)
#define LedState_Toggle()       GPIO_WriteBit(LedState_Port, LedState_Pin, (BitAction) (((uint8_t)GPIO_ReadOutputDataBit(LedState_Port, LedState_Pin) + 1) & 1))


void test_eng_text()
{
  thermo_printf("ABCDEFGHIJKLMNOPQRSTUVWXYZ\r\n");
  thermo_printf("abcdefghijklmnopqrstuvwxyz\r\n");
}

void test_rus_text()
{
  thermo_printf("�����Ũ�������������������������\r\n");
  thermo_printf("�������������������������������\r\n");
}

void test_paper_width()
{
  thermo_printf("012345678901234567890123456789012345678901234567890123456789\r\n");
}

void test_text_align()
{
  thermo_printf("\r\n");

  thermo_set_text_align(ALIGN_LEFT);    // �� ������ ����
  thermo_printf("left align\r\n");
  thermo_printf("����� �����\r\n");

  thermo_set_text_align(ALIGN_MID);     // �� ������
  thermo_printf("middle align\r\n");
  thermo_printf("����� ����������\r\n");

  thermo_set_text_align(ALIGN_RIGHT);   // �� ������� ����
  thermo_printf("right align\r\n");
  thermo_printf("����� ������\r\n");

  thermo_set_text_align(ALIGN_LEFT);    // ���������� ������������ ������ �� ������ ����
  thermo_printf("\r\n");
}

void test_print_modes()
{
  thermo_select_print_mode(0);
  thermo_printf("����� ������ �� ���������\r\n");

  thermo_select_print_mode(PRINTMODE_FONT_B_9x17);
  thermo_printf("����� 9x17\r\n");
  thermo_select_print_mode(0);

/*
  thermo_select_print_mode(PRINTMODE_ANTI_WHITE_ON);
  thermo_printf("����� ANTI-WHITE\r\n");
  thermo_select_print_mode(0);
  
  //thermo_select_print_mode(PRINTMODE_UPSIDE_DOWN_ON);
  //thermo_printf("����� UPSIDE-DOWN\r\n");
  //thermo_select_print_mode(0);
*/

  thermo_upside_down_mode(1);
  thermo_printf("����� UPSIDE-DOWN\r\n");
  thermo_upside_down_mode(0);
  
  thermo_select_print_mode(PRINTMODE_BOLD_MODE_ON);
  thermo_printf("����� BOLD-MODE\r\n");
  thermo_select_print_mode(0);
  
  thermo_select_print_mode(PRINTMODE_DOUBLE_HEIGHT_ON);
  thermo_printf("����� DOUBLE-HEIGHT\r\n");
  thermo_select_print_mode(0);
  
  thermo_select_print_mode(PRINTMODE_DOUBLE_WIDTH_ON);
  thermo_printf("����� DOUBLE-WIDTH\r\n");
  thermo_select_print_mode(0);
  
  //thermo_select_print_mode(PRINTMODE_DELETE_LINE_MODE_ON);
  //thermo_printf("����� DELETE-LINE\r\n");
  //thermo_select_print_mode(0);
  
  thermo_set_rotation_90_mode(1);
  thermo_printf("������� �� 90 ��������\r\n");
  thermo_set_rotation_90_mode(0);

  thermo_set_underlined_height(1);
  thermo_printf("������������� �������� 1\r\n");
  thermo_set_underlined_height(2);
  thermo_printf("������������� �������� 2\r\n");
  thermo_set_underlined_height(0);
}


void test_font_sizes()
{
  for (uint8_t height = 0; height < 8; height++)
  {
    thermo_select_character_size(height, 0);
    thermo_printf("HEIGHT=%d,\r\n", height);
  }

  for (uint8_t width = 0; width < 8; width++)
  {
    thermo_select_character_size(0, width);
    thermo_printf("WIDTH=%d\r\n", width);
  }
  
  // ���������� ������ ������ �� ���������
  thermo_select_character_size(0, 0);
}

void test_text_rotation()
{
  thermo_set_rotation_90_mode(1);
  thermo_printf("TEXT\r\n");
  thermo_set_rotation_90_mode(0);
  thermo_printf("TEXT\r\n");
}

void test_printer_status()
{
  uint8_t status = 0;
  int8_t result = thermo_get_status(&status);
  thermo_printf("RES=%d, STATUS=%d\r\n", result, status);
}

void test_h_v_spacing()
{
  thermo_set_column_character_spacing(0);
  thermo_printf("H spacing=0\r\n");
  thermo_set_column_character_spacing(6);
  thermo_printf("H spacing=6\r\n");
  thermo_set_column_character_spacing(12);
  thermo_printf("H spacing=12\r\n");
  thermo_set_column_character_spacing(16);
  thermo_printf("H spacing=16\r\n");
  thermo_set_column_character_spacing(20);
  thermo_printf("H spacing=20\r\n");
  thermo_set_column_character_spacing(0);

  thermo_set_row_spacing(24);
  thermo_printf("V spacing=24\r\n");
  thermo_set_row_spacing(30);
  thermo_printf("V spacing=30\r\n");
  thermo_set_row_spacing(35);
  thermo_printf("V spacing=35\r\n");
  thermo_set_row_spacing(40);
  thermo_printf("V spacing=40\r\n");
  thermo_set_row_spacing(45);
  thermo_printf("V spacing=45\r\n");
  thermo_set_row_spacing(0);
}

void test_upside_down_mode()
{
  thermo_upside_down_mode(1);
  thermo_printf("UPSIDE DOWN MODE ON\r\n");
  thermo_upside_down_mode(0);
  thermo_printf("UPSIDE DOWN MODE OFF\r\n");
}

void test_underlined_height()
{
  thermo_set_underlined_height(0);
  thermo_printf("UNDERLINED HEIGHT=0\r\n");
  thermo_set_underlined_height(1);
  thermo_printf("UNDERLINED HEIGHT=1\r\n");
  thermo_set_underlined_height(2);
  thermo_printf("UNDERLINED HEIGHT=2\r\n");
  thermo_set_underlined_height(0);
}

void test_custom_char()
{
  uint8_t charBuff[] = 
  { 
    0x80, 0x00, 0x00,   // ������� 1
    0x80, 0x00, 0x00,   // ������� 2
    0xFF, 0xFF, 0xFF,   // ������� 3
    0x00, 0x00, 0x01,   // ������� 4
    0x00, 0x00, 0x01,   // ������� 5
    0x00, 0x00, 0x01,   // ������� 6
    0x00, 0x00, 0x01,   // ������� 7
    0x00, 0x00, 0x01,   // ������� 8
    0xFF, 0xFF, 0xFF,   // ������� 9
    0x80, 0x00, 0x00,   // ������� 10
    0x80, 0x00, 0x00,   // ������� 11
    0x80, 0x00, 0x00,   // ������� 12
  };
  
  int8_t result = thermo_load_custom_char('!', 24, 12, charBuff);
  thermo_custom_char_mode(1);
  thermo_printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\r\n");
  thermo_printf("����� 1\r\n");
  thermo_printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\r\n");
  thermo_printf("����� 2\r\n");
  thermo_printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\r\n");
  thermo_custom_char_mode(0);
  thermo_skip_lines(1);
}

void test_bitmap()
{
  uint8_t buff[192];
  for (uint16_t i = 0; i < 192; i++)
    buff[i] = i & 0xFF;
  
  for (uint16_t i = 0; i < 10; i++)
  {
    thermo_bitmap_fill(buff, 192);
    thermo_bitmap_print(DoubleXY);
  }
}

void test_barcode()
{
  char barCode1[] = "01234567890";
  char barCode2[] = "0123456789012";
  char barCode3[] = "0123456789";

  thermo_set_barcode_width(2);
  thermo_set_barcode_height(32);        // 4 ��
  
  thermo_printf("UPC-A BARCODE:\r\n");
  thermo_print_barcode(UPC_A, strlen(barCode1), (uint8_t *) barCode1);

  thermo_printf("JAN13 BARCODE:\r\n");
  thermo_print_barcode(JAN13, strlen(barCode2), (uint8_t *) barCode2);

  thermo_printf("CODE39 BARCODE:\r\n");
  thermo_print_barcode(CODE39, strlen(barCode1), (uint8_t *) barCode1);
  
  thermo_printf("ITF BARCODE:\r\n");
  thermo_print_barcode(ITF, strlen(barCode3), (uint8_t *) barCode3);
  
  thermo_printf("CODABAR BARCODE:\r\n");
  thermo_print_barcode(CODABAR, strlen(barCode2), (uint8_t *) barCode2);
  
  thermo_printf("CODE93 BARCODE:\r\n");
  thermo_print_barcode(CODE93, strlen(barCode2), (uint8_t *) barCode2);
  
  thermo_printf("CODE128 BARCODE:\r\n");
  thermo_print_barcode(CODE128, strlen(barCode2), (uint8_t *) barCode2);
  
  thermo_printf("UCC-EAN1 BARCODE:\r\n");
  thermo_print_barcode(UCC_EAN1, strlen(barCode2), (uint8_t *) barCode2);
}

void test_qrcode()
{
  char Str[] = "https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ";
  thermo_set_qrcode_correction_level(CorrectionLevel_7);        //CorrectionLevel_25);
  thermo_set_qrcode_data((uint8_t *) Str, strlen(Str));
  thermo_set_qrcode_info_types();

  thermo_set_qrcode_size(1);   // ������ QR-���� (�� 16)
  thermo_set_qrcode_print();
  thermo_set_qrcode_size(2);   // ������ QR-���� (�� 16)
  thermo_set_qrcode_print();
  thermo_set_qrcode_size(3);   // ������ QR-���� (�� 16)
  thermo_set_qrcode_print();
  thermo_set_qrcode_size(5);   // ������ QR-���� (�� 16)
  thermo_set_qrcode_print();
  thermo_set_qrcode_size(8);   // ������ QR-���� (�� 16)
  thermo_set_qrcode_print();
  thermo_set_qrcode_size(10);   // ������ QR-���� (�� 16)
  thermo_set_qrcode_print();
}

void test_heat_modes()
{
  // ��� ��������� HeatPoints ������� �� �����
  thermo_set_print_concentration(9, 80, 2);
  thermo_printf("HeatPoints=9\r\n");

  thermo_set_print_concentration(15, 80, 2);
  thermo_printf("HeatPoints=15\r\n");
  
  thermo_set_print_concentration(25, 80, 2);
  thermo_printf("HeatPoints=25\r\n");

  thermo_set_print_concentration(40, 80, 2);
  thermo_printf("HeatPoints=40\r\n");

  thermo_set_print_concentration(80, 80, 2);
  thermo_printf("HeatPoints=80\r\n");

  thermo_set_print_concentration(160, 80, 2);
  thermo_printf("HeatPoints=160\r\n");

  // ��� ��������� HeatTime ����������� ���������� �����������, �� ��� ��������� ���� 150 �������� �����������
  thermo_set_print_concentration(9, 1, 2);
  thermo_printf("HeatTime=1\r\n");

  thermo_set_print_concentration(9, 5, 2);
  thermo_printf("HeatTime=5\r\n");

  thermo_set_print_concentration(9, 10, 2);
  thermo_printf("HeatTime=10\r\n");

  thermo_set_print_concentration(9, 20, 2);
  thermo_printf("HeatTime=20\r\n");

  thermo_set_print_concentration(9, 30, 2);
  thermo_printf("HeatTime=30\r\n");

  thermo_set_print_concentration(9, 40, 2);
  thermo_printf("HeatTime=40\r\n");

  thermo_set_print_concentration(9, 50, 2);
  thermo_printf("HeatTime=50\r\n");

  thermo_set_print_concentration(9, 60, 2);
  thermo_printf("HeatTime=60\r\n");

  thermo_set_print_concentration(9, 70, 2);
  thermo_printf("HeatTime=70\r\n");

  thermo_set_print_concentration(9, 80, 2);
  thermo_printf("HeatTime=80\r\n");

  thermo_set_print_concentration(9, 130, 2);
  thermo_printf("HeatTime=130\r\n");

  thermo_set_print_concentration(9, 180, 2);
  thermo_printf("HeatTime=180\r\n");

  thermo_set_print_concentration(9, 240, 2);
  thermo_printf("HeatTime=240\r\n");

  thermo_set_print_concentration(9, 255, 2);
  thermo_printf("HeatTime=255\r\n");
  thermo_set_print_concentration(9, 80, 2);

  // ��� ���������� HeatInterval ����������� ���������� �������
  thermo_set_print_concentration(9, 80, 1);
  thermo_printf("HeatInterval=1\r\n");
  
  thermo_set_print_concentration(9, 80, 10);
  thermo_printf("HeatInterval=10\r\n");
  
  thermo_set_print_concentration(9, 80, 25);
  thermo_printf("HeatInterval=25\r\n");
  
  thermo_set_print_concentration(9, 80, 50);
  thermo_printf("HeatInterval=50\r\n");
  
  thermo_set_print_concentration(9, 80, 75);
  thermo_printf("HeatInterval=75\r\n");
  
  thermo_set_print_concentration(9, 80, 100);
  thermo_printf("HeatInterval=100\r\n");
  
  thermo_set_print_concentration(9, 80, 125);
  thermo_printf("HeatInterval=125\r\n");
  
  thermo_set_print_concentration(9, 80, 150);
  thermo_printf("HeatInterval=150\r\n");
  
  thermo_set_print_concentration(9, 80, 175);
  thermo_printf("HeatInterval=175\r\n");
  
  thermo_set_print_concentration(9, 80, 200);
  thermo_printf("HeatInterval=200\r\n");
  
  thermo_set_print_concentration(9, 80, 225);
  thermo_printf("HeatInterval=225\r\n");
  
  thermo_set_print_concentration(9, 80, 255);
  thermo_printf("HeatInterval=255\r\n");
  
  thermo_set_print_concentration(9, 80, 2);
}

//==============================================================================
//
//==============================================================================

void main()
{
  SystemInit();
    
  // ������������� ����� ����������� �� ����� � ��
  gpio_PortClockStart(LedState_Port);
  gpio_SetGPIOmode_Out(LedState_Port, LedState_Pin);
  LedState_Off();
  
  thermal_printer_init(USART1, 9600);

  thermo_printf("---------- start line ----------\r\n");

  //test_paper_width();
  /*
  test_eng_text();
  test_rus_text();
  test_text_align();
  
  test_print_modes();

  test_font_sizes();
  
  test_h_v_spacing();
  
  test_custom_char();
  
  test_bitmap();
  
  test_barcode();
  
  test_qrcode();
  */
  test_heat_modes();
  
  thermo_printf("----------- end line -----------\r\n");
  //thermo_skip_lines(2);
  thermo_printf("\r\n");
  thermo_printf("\r\n");

  while (1)
  {
    // ������ ����������� �� �����
    LedState_Toggle();
    delay_ms(1000);
  }
}
//==============================================================================
